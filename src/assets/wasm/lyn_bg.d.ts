/* tslint:disable */
export const memory: WebAssembly.Memory;
export function init(): void;
export function get_atom_positions(a: number, b: number, c: number): void;
export function __wbindgen_global_argument_ptr(): number;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_free(a: number, b: number): void;
