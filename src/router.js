import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import NotFound from './views/NotFound.vue'
import Docs from './views/Docs.vue'
import Learn from './views/Learn.vue'
import Make from './views/Make.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/docs',
      name: 'docs',
      component: Docs
    },
    {
      path: '/make',
      name: 'make',
      component: Make
    },
    {
      path: '/learn',
      name: 'learn',
      component: Learn
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    { path: '*', component: NotFound }
  ]
})
