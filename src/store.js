import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	docs: {
  		chapters: [
  			{
  				name: 'The Lyn Language Documentation',
  				
  			}
  		]
  	},
    nav: {
      currentItem: '',
      currentModal: 'none'
    }
  },
  mutations: {
    SET_NAV_CURRENT (state, currentItem) {
      state.nav.currentItem = currentItem
    },    
    SET_MODAL_CURRENT (state, currentModal) {
      state.nav.currentModal = currentModal
    }
  },
  getters: {
    getCurrentNavItem: state => {
      return state.nav.currentItem
    }
  },
  actions: {

  }
})
